# Mandarinograd 
中文维诺格拉德模式数据集

Mandarinograd is a collection of 154 Winograd Schemas (WS) in Mandarin Chinese, mainly translated and adapted from English (from [Ernest Davis' collection](https://cs.nyu.edu/faculty/davise/papers/WinogradSchemas/WSCollection.html)).
Winograd Schemas represent particularly hard problems in natural language understanding, related to anaphora resolution.
They are designed to involve common sense reasoning and to limit the biases and artefacts commonly found in natural language understanding datasets.

Mandarinograd这个名字来源于英语单词Mandarin（中文普通话）和Winograd（维诺格拉德）。本数据集包含154个中文维诺格拉德模式，代表了一些在自然语言处理中非常具有挑战性的代词消歧问题。这些模式绝大部分由[英文维诺格拉德模式](https://cs.nyu.edu/faculty/davise/papers/WinogradSchemas/WSCollection.html)翻译和调整而来。理解维诺格拉德模式需要依据常识进行推理，因此，这些数据能够有效衡量自然语言理解系统的常识推理能力，识别系统是否在根据统计数据猜测答案达到理解自然语言的假象。



Mandarinograd is available under different forms.

* The [collection_compact_form.xml file](https://gitlab.com/vanTot/mandarinograd/tree/master/build/collection_compact_form.xml) presents the WS in their traditional form, with the two versions of each WS compacted using the `[_/_]` notation.
* The [collection.xml](https://gitlab.com/vanTot/mandarinograd/blob/master/build/collection.xml) also presents the WS in their traditional form but showing the two versions of each WS separately instead of compacting them.
* The [collection_NLI.txt](https://gitlab.com/vanTot/mandarinograd/tree/master/build/collection_NLI.txt) shows each WS as four NLI instances. Each such instance is a (text, hypothesis) pair annotated with an entailment class (1 = entailment, 0 = no entailment).
* The [collection_disambiguated.txt](https://gitlab.com/vanTot/mandarinograd/tree/master/build/collection_disambiguated.txt) shows WS as two disambiguation problems. Each such problem is a triplet (t, c1, c2) where c1 (resp. c2) is a correct (resp. wrong) disambiguation of text t.

我们提供中文维诺格拉德数据集的以下几种格式：

* [collection_compact_form.xml](https://gitlab.com/vanTot/mandarinograd/tree/master/build/collection_compact_form.xml) 提供紧缩传统格式的维诺格拉德数据集，即每一个模式中包含一对由“/”分割的关键词（比如[小/大]）。每个关键词对应该该模式的一个版本。
* [collection.xml](https://gitlab.com/vanTot/mandarinograd/blob/master/build/collection.xml) 同样提供传统格式的维诺格拉德数据集。但是同一个模式的不同版本被分别列出来，而不是紧缩在一起。
* [collection_NLI.txt](https://gitlab.com/vanTot/mandarinograd/tree/master/build/collection_NLI.txt) 提供维诺格拉德数据集的自然语言推理格式。每一个模式中包含一个（前提，假设）文本对，以及人工标注的类别（1 = 蕴含关系，0 = 冲突）。
* [collection_disambiguated.txt](https://gitlab.com/vanTot/mandarinograd/tree/master/build/collection_disambiguated.txt) 提供维诺格拉德模式的消歧格式。每一个模式是一个三重态（t, c1, c2）。其中c1是文本t消歧后的正确文本，而c2是消歧后的错误文本。

The whole collection is written in simplified Chinese.\
All texts have been tokenised using [Jieba](https://github.com/fxsjy/jieba) and then hand-corrected following [LIVAC's segmentation guidelines](http://sighan.cs.uchicago.edu/bakeoff2005/data/cityu_spec.pdf).\
The files mentioned above use Chinese names. they can be generated with Western names instead using the scripts in the [code folder](https://gitlab.com/vanTot/mandarinograd/tree/master/code).

该数据集使用了简体中文。我们对数据中的文本进行了分词处理。首先使用结巴分词工具进行自动分词，然后根据[香港城市大学提供的中文分词标准](http://sighan.cs.uchicago.edu/bakeoff2005/data/cityu_spec.pdf)进行了手动纠正。以上文件中使用的中文人名由[code folder](https://gitlab.com/vanTot/mandarinograd/tree/master/code)中的程序根据原维诺格拉德模式中的英文人名自动转换生成。