#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =============================================================================
#  Version: 0.1 (November 26, 2019)
#  Author: Timothée Bernard (timothee.bernard@ens-lyon.org)
# =============================================================================

import json

from utils import expand
from convert_names import NameConverter

def auxSchemaVersion(schema_version, name_converter, correspondances):
	text, correspondances = name_converter.convertStr(schema_version['text'], correspondances)
	correct_disambiguation, correspondances = name_converter.convertStr(schema_version['disambiguated'][0], correspondances)
	wrong_disambiguation, correspondances = name_converter.convertStr(schema_version['disambiguated'][1], correspondances)
	
	l = [text, correct_disambiguation, wrong_disambiguation]
	
	return l, correspondances

def generateDisambiguated(western_names=False, corpus_file="../data/main_file.json"):
	l = []

	ws_corpus = json.load(open(corpus_file, "r"))
	name_converter = NameConverter(deactivated=western_names)
	for schema in ws_corpus:
		correspondances = None # Correspondances between Western and Chinese names, for the conversion

		expanded_schema = expand(schema)
		
		item, correspondances = auxSchemaVersion(expanded_schema['special'], name_converter, correspondances)
		l.append(item)
		
		item, correspondances = auxSchemaVersion(expanded_schema['alternate'], name_converter, correspondances)
		l.append(item)
	
	return l

if __name__ == '__main__':
	import argparse
	arg_parser = argparse.ArgumentParser()
	arg_parser.add_argument('-w', '--western_names', help='use Western names instead of Chinese names', action='store_true')
	args = arg_parser.parse_args()
	
	print("#id\ttext\tcorrect disambiguation\twrong disambiguation")
	l = generateDisambiguated(western_names=args.western_names)
	for i, item in enumerate(l):
		(text, correct_disambiguation, wrong_disambiguation) = item
		print("%i\t%s\t%s\t%s" % (i, text, correct_disambiguation, wrong_disambiguation))
