#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =============================================================================
#  Version: 0.1 (November 25, 2019)
#  Author: Timothée Bernard (timothee.bernard@ens-lyon.org)
# =============================================================================

import lxml.etree as ET #import xml.etree.ElementTree as ET
import json

from utils import expand
from convert_names import NameConverter

# Only used for the expanded (non compact) presentation
def addSchemaVersion(parent, schema_version, name_converter, correspondances):
	text, correspondances = name_converter.convertStr(schema_version['text'], correspondances)
	question, correspondances = name_converter.convertStr(schema_version['question'], correspondances)
	correct_answer, correspondances = name_converter.convertStr(schema_version['answers'][0], correspondances)
	wrong_answer, correspondances = name_converter.convertStr(schema_version['answers'][1], correspondances)
	
	text_XML = ET.SubElement(parent, 'text')
	text_XML.text = text
	
	question_XML = ET.SubElement(parent, 'question')
	question_XML.text = question
	
	correct_answer_XML = ET.SubElement(parent, 'correct_answer')
	correct_answer_XML.text = correct_answer
	
	wrong_answer_XML = ET.SubElement(parent, 'wrong_answer')
	wrong_answer_XML.text = wrong_answer
	
	return correspondances

def addSchema(parent, idx, schema, name_converter, compact_form=False):
	schema_XML = ET.SubElement(parent, 'schema')
	
	schema_XML.set('id', str(idx))
	#idML = ET.SubElement(schema_XML, 'id')
	#id.text = str(idx)
	
	correspondances = None # Correspondances between Western and Chinese names, for the conversion

	if(compact_form):
		text, correspondances = name_converter.convertStr(schema['text'], correspondances)
		question, correspondances = name_converter.convertStr(schema['question'], correspondances)
		answer_A, correspondances = name_converter.convertStr(schema['answers'][0], correspondances)
		answer_B, correspondances = name_converter.convertStr(schema['answers'][1], correspondances)
		
		text_XML = ET.SubElement(schema_XML, 'text')
		text_XML.text = text
		
		question_XML = ET.SubElement(schema_XML, 'question')
		question_XML.text = question
		
		answer_A_XML = ET.SubElement(schema_XML, 'answer_A')
		answer_A_XML.text = answer_A
		
		answer_B_XML = ET.SubElement(schema_XML, 'answer_B')
		answer_B_XML.text = answer_B
	else:
		expanded_schema = expand(schema)
		
		special_version_XML = ET.SubElement(schema_XML, 'special_version')
		correspondances = addSchemaVersion(special_version_XML, expanded_schema['special'], name_converter, correspondances)
	
		alternate_version_XML = ET.SubElement(schema_XML, 'alternate_version')
		correspondances = addSchemaVersion(alternate_version_XML, expanded_schema['alternate'], name_converter, correspondances)
	
	origin_id = schema['origin']['id'][1] # The id according to "https://cs.nyu.edu/faculty/davise/papers/WinogradSchemas/WSCollection.html"
	modification = schema['origin']['modification']
	
	origin_XML = ET.SubElement(schema_XML, 'origin')
	origin_id_XML = ET.SubElement(origin_XML, 'id')
	origin_id_XML.text = str(origin_id) if(origin_id > 0) else "NEW"
	if(modification is not None):
		origin_modification_XML = ET.SubElement(origin_XML, 'modification')
		origin_modification_XML.text = modification

def generateXML(compact_form=False, western_names=False, corpus_file="../data/main_file.json"):
	collection_XML = ET.Element('collection')

	ws_corpus = json.load(open(corpus_file, "r"))
	name_converter = NameConverter(deactivated=western_names)
	for idx, schema in enumerate(ws_corpus):
		addSchema(collection_XML, idx, schema, name_converter, compact_form)
	
	return collection_XML

if __name__ == '__main__':
	import argparse
	arg_parser = argparse.ArgumentParser()
	arg_parser.add_argument('-c', '--compact_form', help='present the WS in their compacted (traditional) form', action='store_true')
	arg_parser.add_argument('-w', '--western_names', help='use Western names instead of Chinese names', action='store_true')
	args = arg_parser.parse_args()
	
	collection = generateXML(compact_form=args.compact_form, western_names=args.western_names)
	string = ET.tostring(collection, encoding='unicode', method='xml', pretty_print=True)
	print(string)
