#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =============================================================================
#  Version: 0.1 (September 24, 2019)
#  Author: Timothée Bernard (timothee.bernard@ens-lyon.org)
# =============================================================================

import re

# Used to convert Western names to Chinese names
# Once loaded, `convertStr` can be used to convert strings. As a Winograd Schema corresponds to multiple strings (text, question, etc.), consistency in ensured with the `correspondances` argument. This argument is a dictionary that is (potientially) updated with each call to `convertStr`.
# Western names are defined by `regex`. In particular, they cannot contain any space (but underscores are allowed).
class NameConverter:
	regex = re.compile("[a-zA-Z_\.]+") # The same as in extract_names.py

	def __init__(self, name_file_name="../data/name_replacement.txt", deactivated=False):
		self.deactivated = deactivated
	
		self.chinese_names = {'F': set(), 'M': set()} # The sets will be converted to lists soon
		self.western_gender = {} # Name to gender
		self.specific_translations = {}
		
		# Sections definition when loading the file
		FEMALE_CHINESE = 0
		MALE_CHINESE = 1
		WESTERN_NAMES = 2
		SPECIFIC = 3
		
		section = -1
		with open(name_file_name, 'r') as file:
			for line in file:
				line = line.rstrip()
				#print(line)
				if(line.startswith('# ---')):
					section += 1
					continue
				
				if(section == FEMALE_CHINESE):
					self.chinese_names['F'].add(line)
					continue
				
				if(section == MALE_CHINESE):
					self.chinese_names['M'].add(line)
					continue
				
				if(section == WESTERN_NAMES):
					l = line.split('\t')
					self.western_gender[l[0]] = l[1] # "F" or "M"
					continue
				
				if(section == SPECIFIC):
					l = line.split('\t')
					self.specific_translations[l[0]] = l[1]
					continue
				
				input("NameConverter.__init__: unexpected section '%s'." % section)
				break
		
		self.chinese_names['F'] = list(self.chinese_names['F']) # List, so we can iterate through it consistently
		self.chinese_names['M'] = list(self.chinese_names['M']) # List, so we can iterate through it consistently
		
#		input(self.chinese_names)
#		input(self.western_gender)
		
		self.counters = {'F': 0, 'M': 0} # See `convertStr` below to understand
	
	# The basic rule is that the same Chinese name shouldn't be used in the same WS for two different western names; to implement this, we just iterate through the two lists of Chinese names (M and F)
	def convertStr(self, s, correspondances=None):
		if(self.deactivated): return (s, correspondances)
		
		if(correspondances is None): correspondances = {}
		
		# Updates the en to cz correspondances
		names = set(self.regex.findall(s))
		for name in names:
			if(name in correspondances): continue;
			
			translation = self.specific_translations.get(name)
			if(translation is not None):
				correspondances[name] = translation
				continue
			
			gender = self.western_gender.get(name)
			if(gender is not None):
				correspondances[name] = self.chinese_names[gender][self.counters[gender]]
				self.counters[gender] = (self.counters[gender] + 1) % len(self.chinese_names[gender])
				continue
		
		# Actual replacement
		for en, cz in correspondances.items():
			s = s.replace(en, cz)
		
		return (s, correspondances)
	
	def convertDict(self, d):
		if(self.deactivated): return (s, correspondances)
		
		result = dict()
		correspondances = None
		for k, v in d.items():
			s, correspondances = self.convertStr(v, correspondances)
			result[k] = s
		
		return result

if __name__ == '__main__':
	name_converter = NameConverter()

	from utils import expand
	import json
	
	corpus_file = "../data/ws_zh_tokenized.json"
	ws_corpus = json.load(open(corpus_file, "r"))
	
	for schema in ws_corpus:
		expanded_schema = expand(schema)
		
		# For the special version
		schema_version = expanded_schema['special']
		
		correspondances = None

		s, correspondances = name_converter.convertStr(schema_version['text'], correspondances)
		print(s)
		
		s, correspondances = name_converter.convertStr(schema_version['question'], correspondances)
		print(s)
		
		s, correspondances = name_converter.convertStr(schema_version['answers'][0], correspondances)
		print(s)
		
		s, correspondances = name_converter.convertStr(schema_version['answers'][1], correspondances)
		print(s)
		
		# For the alternate version
		schema_version = expanded_schema['alternate']
		
		#correspondances = None # Here we use the same conversion for both versions
		
		s, correspondances = name_converter.convertStr(schema_version['text'], correspondances)
		print(s)
		
		s, correspondances = name_converter.convertStr(schema_version['question'], correspondances)
		print(s)
		
		s, correspondances = name_converter.convertStr(schema_version['answers'][0], correspondances)
		print(s)
		
		s, correspondances = name_converter.convertStr(schema_version['answers'][1], correspondances)
		print(s)
		
		input()
