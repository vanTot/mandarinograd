#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =============================================================================
#  Version: 0.1 (September 5, 2019)
#  Author: Timothée Bernard (timothee.bernard@ens-lyon.org)
# =============================================================================

import re

pattern = r"\[([^/]*)/([^]]*)\]"

def getSpecial(string):
	if(('/' in string) and ('[' not in string)):
		return string.split('/')[0]
	return re.sub(pattern, r"\1", string)

def getAlternate(string):
	if(('/' in string) and ('[' not in string)):
		return string.split('/')[1]
	return re.sub(pattern, r"\2", string)

def expand_aux_PMI(PMI_field, special):
	if(PMI_field is None): return None;
	
	if(special):
		PMI_pred = PMI_field[0][0]
		PMI_entities = PMI_field[1][0]
		return [PMI_pred, PMI_entities]
	else: # alternate
		PMI_pred = PMI_field[0][0] if(len(PMI_field[0]) == 1) else PMI_field[0][1]
		PMI_entities = PMI_field[1][1] if(len(PMI_field[1]) == 2) else list(reversed(PMI_field[1][0]))
		return [PMI_pred, PMI_entities]

# Expands a schema from the usual compact notation
def expand(schema):
	# 'Special' version
	PMI = expand_aux_PMI(schema['PMI'], special=True)
	if('PMI-improved' in schema): PMI_improved = expand_aux_PMI(schema['PMI-improved'], special=True)
	
	specialVersion = {
		'text': getSpecial(schema['text']),
		'question': getSpecial(schema['question']),
		'answers': [getSpecial(schema['answers'][0]), getSpecial(schema['answers'][1])], # Right then wrong answers
		'NLI': schema['NLI'][0], # Right then wrong entailment candidates
		'disambiguated': schema['disambiguated'][0], # Right then wrong entailment candidates
		'PMI': PMI,
	}
	if('PMI-improved' in schema): specialVersion['PMI-improved'] = PMI_improved
	
	# 'Alternate' version
	PMI = expand_aux_PMI(schema['PMI'], special=False)
	if('PMI-improved' in schema): PMI_improved = expand_aux_PMI(schema['PMI-improved'], special=False)
	
	NLI = schema['NLI'][1] if(len(schema['NLI']) == 2) else list(reversed(schema['NLI'][0]))
	alternateVersion = {
		'text': getAlternate(schema['text']),
		'question': getAlternate(schema['question']),
		'answers': [getAlternate(schema['answers'][1]), getAlternate(schema['answers'][0])], # Right then wrong answers
		'NLI': NLI, # Right then wrong entailment candidates
		'disambiguated': schema['disambiguated'][1], # Right then wrong entailment candidates
		'PMI': PMI,
	}
	if('PMI-improved' in schema): alternateVersion['PMI-improved'] = PMI_improved
	
	return {
		'special': specialVersion,
		'alternate': alternateVersion,
	}
