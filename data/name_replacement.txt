# --- Female Chinese first names ---
婷婷
李静
刘爽
品妤
丽华
丽丽
金丽
芳芳
李敏
王娜
# --- Male Chinese first names ---
小明
老王
俊杰
志明
赵磊
李广
张杰
建国
许巍
张锋
# --- Western first names (F/M) ---
Amy	F
Arnold	M
Dan	M
Timothy	M
Lily	F
Grace	F
Mark	M
Paul	M
Esther	F
Ethan	M
Mary	F
Martin	M
Susan	F
Jackson	M
Sam	M
Janie	F
Kevin	M
Ann	F
Tommy	M
Charlie	M	It is used as a male name in one of the English WS.
Elizabeth	F
Sue	F
Frank	M
Fred	M
Luke	M
Beth	F
Bill	M
George	M
Bert	M
Andrea	F	It is used as a female name in one of the English WS.
Adam	M
Eric	M
Toby	M
Anne	F
Robert	M
Tom	M
Bob	M
Billy	M
Joan	F
Timmy	M
Alice	F
Jane	F
Pam	F
Jim	M
Joe	M
James	M
Joey	M
Carl	M
Donna	F
Anna	F
Ray	M
Sid	M
Carol	F
Jade	F
Eva	F
Steve	M
Sara	F
John	M
Pete	M
Ollie	M
Rebecca	F
David	M
Emma	F
Lucy	F
Ralph	M
Kate	F
Sally	F
# --- Specific ---
Dr._Adams	王 医生à	last name (Wang)
Sam_Goodman	王伟
Dr._Stewart	张 医生	last name (Zhang)
Thomson	王建国	first name, because in Chinese we would not use last names in the corresponding WS
Cooper	刘志华	first name, because in Chinese we would not use last names in the corresponding WS
Tina	王娜	easy to pronounce
Terpsichore	訾涵	quite hard to pronounce
Troy	东莞	city name (Dongguan)
Sparta	天津	city name (Tianjing)
Springfield	东莞	city name (Dongguan)
Franklin	天津	city name (Tianjing)
Yakutsk	雅库茨克
Kamtchatka	堪察加半島
Laputa	勒皮他	The flying island described in the 1726 book /Gulliver's Travels/ by Jonathan Swift.
Madonna	麦当娜	usual translation of the singer's name
Ovid	吴承恩	Wu Cheng'en (150*-158*), instead of 奥维德 (Ovid)
Shakespeare	鲁迅	Lu Xun (1881-1936), instead of 莎士比亚 (Shakespeare)
Goethe	韩寒	Han Han (1982-), instead of 歌德 (Goethe)
Kirilov	基里洛夫	phonetic transcription
Shatov	沙托夫	phonetic transcription
